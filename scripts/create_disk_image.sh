#!/bin/sh

set -e

# shellcheck disable=SC1091,SC2015
[ -e "${LIVE_BUILD}/scripts/build.sh" ] && . "${LIVE_BUILD}/scripts/build.sh" || . /usr/lib/live/build.sh

DISKSIZE="${1:-5120}"
DISKSIZE="$((DISKSIZE / 16))"

Echo_message "Creating disk image..."
dd if=/dev/zero of=disk.img bs=16M count="${DISKSIZE}" status=progress
printf 'g\nn\n\n\n+64M\nt\n1\nn\n\n\n+1536M\nn\n\n\n\nw\n' | fdisk disk.img
LODEVICE="$(losetup --show -Pf disk.img)"
mkfs.vfat "${LODEVICE}p1"
mkfs.ext4 "${LODEVICE}p2"
mkfs.ext4 "${LODEVICE}p3"

Echo_message "Copying OS to disk image..."
MNTDIR=".tmpmnt"
mkdir -p "$MNTDIR"
mount "${LODEVICE}p3" "${MNTDIR}"
cp -a binary/* "${MNTDIR}"
mount "${LODEVICE}p2" "${MNTDIR}/boot"
cp -a boot/* "${MNTDIR}/boot"
mkdir -p "${MNTDIR}/boot/efi"
mount "${LODEVICE}p1" "${MNTDIR}/boot/efi"

Echo_message "Installing grub..."
arch-chroot "${MNTDIR}" grub-install --no-uefi-secure-boot --no-nvram --force-extra-removable
BOOT_UUID="$(blkid -o value -s UUID "${LODEVICE}p2")"
EFIBIN=binary/usr/lib/grub/arm64-efi/monolithic/grubaa64.efi
cp "${EFIBIN}" "${MNTDIR}/boot/efi/EFI/BOOT/BOOTAA64.EFI"
cp "${EFIBIN}" "${MNTDIR}/boot/efi/EFI/debian"
cat <<EOF >"${MNTDIR}/boot/efi/EFI/debian/grub.cfg"
search --fs-uuid --set=root ${BOOT_UUID}
set prefix=(\$root)'/grub'
configfile \$prefix/grub.cfg
EOF
arch-chroot "${MNTDIR}" update-grub

Echo_message "Generating /etc/fstab"
ROOT_UUID="$(blkid -o value -s UUID "${LODEVICE}p3")"
ESP_UUID="$(blkid -o value -s UUID "${LODEVICE}p1")"
cat <<EOF >fstab
UUID=${ROOT_UUID} / ext4 x-systemd.growfs,defaults 0 1
UUID=${BOOT_UUID} /boot ext4 defaults 0 0
UUID=${ESP_UUID} /boot/efi vfat defaults 0 0
EOF
mv fstab "${MNTDIR}/etc"

Echo_message "Extracting ESP directory"
mkdir -p esp
cp -a "${MNTDIR}/boot/efi"/* esp

umount "${MNTDIR}/boot/efi" "${MNTDIR}/boot" "${MNTDIR}"
rm -rf "$MNTDIR"

Echo_message "Extracting boot and root partition images..."
dd if="${LODEVICE}p2" of=boot.img bs=4M status=progress
dd if="${LODEVICE}p3" of=root.img bs=4M status=progress

losetup -d "${LODEVICE}"

echo "${ESP_UUID}" >esp_uuid
