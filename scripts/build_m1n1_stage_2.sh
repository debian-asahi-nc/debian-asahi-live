#!/bin/bash

# Build stage 2 m1n1 without update-m1n1, which would try to mount the ESP

set -e

echo_no_manual() {
    echo "This script is meant to be run automatically by 'lb build'!" >&2
}

if ! [ -e "binary/usr/lib/m1n1/m1n1.bin" ]; then
    echo "Error: m1n1.bin not found in binary image." >&2
    echo_no_manual
    exit 1
fi

if ! [ -e "binary/usr/lib/u-boot-asahi/u-boot-nodtb.bin" ]; then
    echo "Error: u-boot-nodtb.bin not found in binary image." >&2
    echo_no_manual
    exit 1
fi

DTBS="$(find binary -type d -wholename "binary/usr/lib/linux-image-*asahi*/apple")"
if [ ! -d "$DTBS" ]; then
    echo "Error: Apple *.dtb files not found in binary image." >&2
    echo_no_manual
    exit 1
fi

mkdir -p esp/m1n1
# shellcheck disable=2046
cat binary/usr/lib/m1n1/m1n1.bin \
    $(find "$DTBS" -name "*.dtb") \
    <(gzip -c binary/usr/lib/u-boot-asahi/u-boot-nodtb.bin) \
    >esp/m1n1/boot.bin
