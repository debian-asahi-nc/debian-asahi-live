#!/usr/bin/env python3

import hashlib
import json
import os
import shutil
import sys


def os_entry(name, default_os_name, package_name, icon, efi_uuid, efisize, bootsize, rootsize):
    # autopep8: off
    volume_id = f"0x{efi_uuid.replace('-', '').lower()}"
    # autopep8: on
    return {
        "name": f"{name}",
        "default_os_name": f"{default_os_name}",
        "boot_object": "m1n1.bin",
        "next_object": "m1n1/boot.bin",
        "package": f"{package_name}.zip",
        "icon": f"{icon}",
        "supported_fw": ["12.3", "12.3.1", "13.5"],
        "partitions": [
            {
                "name": "EFI",
                "type": "EFI",
                "size": f"{efisize}B",
                "format": "fat",
                "volume_id": f"{volume_id}",
                "copy_firmware": True,
                "copy_installer_data": True,
                "source": "esp"
            },
            {
                "name": "Boot",
                "type": "Linux",
                "size": f"{bootsize}B",
                "image": "boot.img"
            },
            {
                "name": "Root",
                "type": "Linux",
                "size": f"{rootsize}B",
                "expand": True,
                "image": "root.img"
            }
        ]
    }


def debian_name(distribution, configuration):
    return f'Debian Asahi GNU/Linux {distribution} {configuration}'


def debian_entry(distribution, configuration, package_name, efi_uuid, efisize, bootsize, rootsize):
    return os_entry(
        debian_name(distribution, configuration),
        f'Debian GNU/Linux {configuration}',
        package_name, 'debian.icns', efi_uuid,
        efisize, bootsize, rootsize
    )


if __name__ == '__main__':
    installer_data_file = 'installer_data.json'
    if not os.path.exists(installer_data_file):
        shutil.copy('data/installer_data.json', installer_data_file)

    with open(installer_data_file, 'r') as fin:
        installer_data = json.load(fin)

    data = sys.argv[1:]
    entry_name = debian_name(data[0], data[1])
    entry = debian_entry(*data)

    for i in range(len(installer_data['os_list'])):
        if installer_data['os_list'][i]['name'] == entry_name:
            installer_data['os_list'][i] = entry
            break
    else:
        installer_data['os_list'].insert(0, entry)

    with open(installer_data_file, 'w') as fout:
        json.dump(installer_data, fout, indent=4)
