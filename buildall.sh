#!/bin/sh

set -e

if [ "$(id -u)" -ne 0 ]; then
    echo "Root privileges needed!"
    exit 1
fi

while [ $# -gt 0 ]; do
    case "$1" in
    --configs)
        DEBIAN_ASAHI_CONFIGS="$2"
        shift
        ;;
    --installer-only)
        DEBIAN_ASAHI_INSTALLER_CHOICES="1"
        ;;
    --no-installer)
        DEBIAN_ASAHI_INSTALLER_CHOICES="0"
        ;;
    *)
        echo "Error: option $1 not recognized" >&2
        exit 1
        ;;
    esac
    shift
done

DEBIAN_ASAHI_INSTALLER_CHOICES="${DEBIAN_ASAHI_INSTALLER_CHOICES:-"0 1"}"
DEBIAN_ASAHI_CONFIGS="${DEBIAN_ASAHI_CONFIGS:-"console kde gnome"}"

for DEBIAN_ASAHI_INSTALLER in ${DEBIAN_ASAHI_INSTALLER_CHOICES}; do
    export DEBIAN_ASAHI_INSTALLER
    for DEBIAN_ASAHI_CONFIG in ${DEBIAN_ASAHI_CONFIGS}; do
        export DEBIAN_ASAHI_CONFIG
        lb config
        lb build
    done
done

lb clean
