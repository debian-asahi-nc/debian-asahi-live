# debian-asahi-live

## Build instructions

### Live image

```shell
sudo apt-get update
sudo apt-get install -y git live-build
sudo DEBIAN_ASAHI_CONFIG=$CONFIG lb config
sudo lb build
```

### Asahi-installer package

```shell
sudo apt-get update
sudo apt-get install -y arch-install-scripts dosfstools fdisk git live-build python3 zip
sudo DEBIAN_ASAHI_CONFIG=$CONFIG DEBIAN_ASAHI_INSTALLER=1 lb config
sudo lb build
```

### Valid configurations

```shell
DEBIAN_ASAHI_CONFIG=console  # No desktop environment (console-only)
DEBIAN_ASAHI_CONFIG=gnome    # GNOME desktop environment
DEBIAN_ASAHI_CONFIG=kde      # KDE desktop environment
```
